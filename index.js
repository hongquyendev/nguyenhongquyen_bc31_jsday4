/**Bài tập 1
 * input: nhập 3 số
 * output: sắp xếp theo thứ tự tăng dần
 * logic: vd cho 3 số a, b, c
 *      sử dụng if so sánh a và b
 *        Nếu a > b thì so ssanh a và c, nếu a > c thì so sánh b và c, nếu b > c thì xuất kết quả c < b < a, ngược lại thì xuất kết quả b < c < a
 *                                        nếu a < c thì xuất kết quả b < a < c
 *        Nếu a < b thì so sánh a và c, nếu a > c  thì xuất kết quả c < a < b
 *                                      nếu a < c thì so sánh b và c, nếu b > c thì xuất kết quả a < c < b, ngược lại thì xuất kết quả a < b < c
 *
 *
 */
function run() {
  var sothu1, sothu2, sothu3, sapxep;
  sothu1 = document.getElementById("sothu1").value * 1;
  sothu2 = document.getElementById("sothu2").value * 1;
  sothu3 = document.getElementById("sothu3").value * 1;
  if (sothu1 > sothu2) {
    if (sothu1 > sothu3) {
      if (sothu2 > sothu3) {
        document.getElementById(
          "sapxep"
        ).innerHTML = `<div>Thứ tự tăng dần là: ${sothu3} < ${sothu2} < ${sothu1} </div>`;
      } else {
        document.getElementById(
          "sapxep"
        ).innerHTML = `<div>Thứ tự tăng dần là: ${sothu2} < ${sothu3} < ${sothu1} </div>`;
      }
    } else {
      document.getElementById(
        "sapxep"
      ).innerHTML = `<div>Thứ tự tăng dần là: ${sothu2} < ${sothu1} < ${sothu3} </div>`;
    }
  } else {
    if (sothu1 > sothu3) {
      document.getElementById(
        "sapxep"
      ).innerHTML = `<div>Thứ tự tăng dần là: ${sothu3} < ${sothu1} < ${sothu2} </div>`;
    } else {
      if (sothu2 > sothu3) {
        document.getElementById(
          "sapxep"
        ).innerHTML = `<div>Thứ tự tăng dần là: ${sothu1} < ${sothu3} < ${sothu2} </div>`;
      } else {
        document.getElementById(
          "sapxep"
        ).innerHTML = `<div>Thứ tự tăng dần là: ${sothu1} < ${sothu2} < ${sothu3} </div>`;
      }
    }
  }
}

function guiloichao() {
  var people = document.getElementById("people").value;
  switch (people) {
    case "B":
      document.getElementById("xinchao").innerHTML = `<div>Xin chào Ba! </div>`;
      break;
    case "M":
      document.getElementById("xinchao").innerHTML = `<div>Xin chào Mẹ! </div>`;
      break;
    case "A":
      document.getElementById(
        "xinchao"
      ).innerHTML = `<div>Xin chào Anh trai! </div>`;
      break;
    case "E":
      document.getElementById(
        "xinchao"
      ).innerHTML = `<div>Xin chào Em gái! </div>`;
      break;
    case "khac":
      document.getElementById(
        "xinchao"
      ).innerHTML = `<div>Xin chào Người lạ đẹp trai! </div>`;
      break;
  }
}

function dem() {
  var sothu1 = document.getElementById("sothu1_3").value * 1;
  var sothu2 = document.getElementById("sothu2_3").value * 1;
  var sothu3 = document.getElementById("sothu3_3").value * 1;
  var sochan = 0;
  var sole;
  if (sothu1 % 2 == 0) {
    sochan++;
  }
  if (sothu2 % 2 == 0) {
    sochan++;
  }
  if (sothu3 % 2 == 0) {
    sochan++;
  }
  sole = 3 - sochan;
  document.getElementById(
    "chanle"
  ).innerHTML = `<div>Có ${sochan} số chẵn - ${sole} Số lẻ</div>`;
}

function dudoan() {
  var canhthu1 = document.getElementById("canhthu1").value * 1;
  var canhthu2 = document.getElementById("canhthu2").value * 1;
  var canhthu3 = document.getElementById("canhthu3").value * 1;
  if (canhthu1 == canhthu2) {
    if (canhthu2 == canhthu3) {
      document.getElementById("ketqua").innerHTML = `<div>Tam giác đều</div>`;
    } else {
      document.getElementById("ketqua").innerHTML = `<div>Tam giác cân</div>`;
    }
  } else {
    if (canhthu2 == canhthu3) {
      document.getElementById("ketqua").innerHTML = `<div>Tam giác cân</div>`;
    } else {
      if (canhthu1 * canhthu1 + canhthu2 * canhthu2 == canhthu3 * canhthu3) {
        document.getElementById(
          "ketqua"
        ).innerHTML = `<div>Tam giác vuông</div>`;
      } else {
        if (canhthu2 * canhthu2 + canhthu3 * canhthu3 == canhthu1 * canhthu1) {
          document.getElementById(
            "ketqua"
          ).innerHTML = `<div>Tam giác vuông</div>`;
        } else {
          if (
            canhthu3 * canhthu3 + canhthu1 * canhthu1 ==
            canhthu2 * canhthu2
          ) {
            document.getElementById(
              "ketqua"
            ).innerHTML = `<div>Tam giác vuông</div>`;
          } else {
            document.getElementById(
              "ketqua"
            ).innerHTML = `<div>Tam giác khác</div>`;
          }
        }
      }
    }
  }
}
